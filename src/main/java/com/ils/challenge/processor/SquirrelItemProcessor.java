package com.ils.challenge.processor;

import com.ils.challenge.model.Squirrel;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;

@Slf4j
public class SquirrelItemProcessor implements ItemProcessor<Squirrel, Squirrel> {
    @Override
    public Squirrel process(@NonNull Squirrel item) {
        log.info("Processed record : " + item.getUnique_squirrel_id());
        return item;
    }
}