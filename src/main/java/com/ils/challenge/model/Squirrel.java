package com.ils.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Squirrel {
    private String x;
    private String y;
    private String unique_squirrel_id;
    private String hectare;
    private String shift;
    private String date;
    private String hectare_squirrel_number;
    private String age;
    private String primary_fur_color;
    private String highlight_fur_color;
    private String color_combination;
    private String color_notes;
    private String location;
    private String measurement;
    private String specific_location;
    private String running;
    private String chasing;
    private String climbing;
    private String eating;
    private String foraging;
    private String other_activities;
    private String kuks;
    private String quaas;
    private String moans;
    private String tail_flags;
    private String tail_twitches;
    private String approaches;
    private String indifferent;
    private String runs_from;
    private String other_interactions;
    private String lat_long;
}
